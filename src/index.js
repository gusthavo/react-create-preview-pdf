import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { PageHeightProvider } from "./providers/pageHeight";

ReactDOM.render(
  <React.StrictMode>
    <PageHeightProvider>
      <App />
    </PageHeightProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
