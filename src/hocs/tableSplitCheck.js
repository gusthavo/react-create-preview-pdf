import React, { useRef, useState } from "react";
import TableItems from "../components/TableItems";
import { usePageHeight } from "../providers/pageHeight";

export default function TableSplitCheck() {
  const [tableItems, setTableItems] = useState([
    [
      `<tr>
    <td>IMPLANT</td>
    <td>46</td>
    <td></td>
    <td>OK</td>
    <td></td>
  </tr>`,
      `<tr>
    <td>CARIES</td>
    <td>11,12,21,22</td>
    <td>Distal, Lingual</td>
    <td></td>
    <td></td>
  </tr>`,
      `<tr>
    <td>Severely damaged</td>
    <td>43</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>`,
      `<tr>
    <td>Missing Teeth</td>
    <td>34</td>
    <td></td>
    <td>OK</td>
    <td></td>
  </tr>`,
      `<tr>
    <td>Discolored Teeth</td>
    <td>14,15,24</td>
    <td></td>
    <td>OK</td>
    <td></td>
  </tr>`,
    ],
  ]);
  const [totalTables, setTotalTables] = useState(1);
  const {
    defaultPageHeight,
    currentPageHeight,
    setPageData,
    pageData,
    totalPages,
    setTotalPages,
  } = usePageHeight();

  const rowHeight = 20;
  const tableHeight = 103;

  const tableHeader = () => (
    <tr>
      <th>&nbsp;</th>
      <th>Teeth</th>
      <th>Surface</th>
      <th>Degree/Status</th>
      <th>Observations</th>
    </tr>
  );

  console.log();
  console.log(tableHeader());

  return (
    <TableItems tableHeader={tableHeader()} tableContent={tableItems[0]} />
  );
}
