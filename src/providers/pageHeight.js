import React, { createContext, useState, useContext, useEffect } from "react";
import Molocclusion from "../components/Molocclusion";
import Odontogram from "../components/Odontogram";
import TableItems from "../components/TableItems";

export const PageHeightContext = createContext(60);

export const PageHeightProvider = (props) => {
  const [currentPageHeight, setCurrentPageHeight] = useState(60);
  const [pageData, setPageData] = useState([
    [],
    [<Molocclusion />, <Odontogram />, <Molocclusion />, <TableItems />],
  ]);

  const [totalPages, setTotalPages] = useState(1);

  const defaultPageHeight = 860;

  return (
    <PageHeightContext.Provider
      value={{
        currentPageHeight,
        setCurrentPageHeight,
        defaultPageHeight,
        pageData,
        setPageData,
        totalPages,
        setTotalPages,
      }}
    >
      {props.children}
    </PageHeightContext.Provider>
  );
};

export const usePageHeight = () => useContext(PageHeightContext);
