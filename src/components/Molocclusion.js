import Occlusion from "../assets/occlusion-1.png";

import "./molocclusion.css";

export default function Molocclusion() {
  return (
    <div className="molocclusion">
      <p>
        Class II Molocclusion - This means your upper teeth are more advanced
        than your lower teeth then you bite
      </p>
      <table>
        <thead>
          <tr>
            <th>Oclusão Ideal - Classe I</th>
            <th>A situação - Classe III</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <img src={Occlusion} alt="Oclusão Ideal - Classe I" />
            </td>
            <td>
              <img src={Occlusion} alt="Oclusão Ideal - Classe II" />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
