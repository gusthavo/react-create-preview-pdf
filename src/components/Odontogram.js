import OdontogramImg from "../assets/odontogram.png";

import "./odontogram.css";

export default function Odontogram() {
  return (
    <div className="odontogram">
      <img src={OdontogramImg} alt="Odontogram" />
    </div>
  );
}
