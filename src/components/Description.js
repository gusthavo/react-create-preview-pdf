export default function Description() {
  const customText = [
    `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis
    purus sit amet augue maximus, vel aliquam neque ultrices. Etiam a
    scelerisque justo, sit amet pretium nisl. Mauris varius, nisl non
    vestibulum dictum, dolor elit tristique urna, eu faucibus justo arcu at
    ipsum. Aliquam erat volutpat.`,

    `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis
    purus sit amet augue maximus, vel aliquam neque ultrices.`,

    `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis
    purus sit amet augue maximus, vel aliquam neque ultrices. Etiam a
    scelerisque justo, sit amet pretium nisl. Mauris varius, nisl non
    vestibulum dictum, dolor elit tristique urna, eu faucibus justo arcu at
    ipsum. Aliquam erat volutpat.
    
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis
        purus sit amet augue maximus, vel aliquam neque ultrices. Etiam a
        scelerisque justo, sit amet pretium nisl. Mauris varius, nisl non
        vestibulum dictum, dolor elit tristique urna, eu faucibus justo arcu at
        ipsum. Aliquam erat volutpat.`,
  ];

  function getCustomText() {
    return customText[Math.floor(Math.random() * customText.length)];
  }

  return (
    <div className="rectangle">
      <b>title</b>
      <p>{getCustomText()}</p>
    </div>
  );
}
