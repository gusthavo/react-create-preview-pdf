import React, { useRef, useState } from "react";
import Description from "./components/Description";
import Molocclusion from "./components/Molocclusion";
import Odontogram from "./components/Odontogram";
import TableItems from "./components/TableItems";
import "./App.css";

import { usePageHeight } from "./providers/pageHeight";
import TableSplitCheck, { tableSplitCheck } from "./hocs/tableSplitCheck";

function App() {
  const itemToBeAdded = [
    <Description />,
    <Molocclusion />,
    <Odontogram />,
    <TableItems />,
  ];

  const pagesRef = useRef([]);
  const {
    setCurrentPageHeight,
    currentPageHeight,
    defaultPageHeight,
    pageData,
    setPageData,
    totalPages,
    setTotalPages,
  } = usePageHeight();

  const lastItemHeightRef = useRef(0);

  function getRandomItem() {
    return itemToBeAdded[Math.floor(Math.random() * itemToBeAdded.length)];
  }

  /**
   * Create associative array
   * [1 => [rectangle,rectangle,rectangle,rectangle,rectangle,]]
   * [2 => [rectangle,rectangle, ]]
   * page1 = [1,2,3,4]
   * page2 = [5,6,7,8]
   */
  function addItem() {
    const newData = [...pageData];
    // const itemToBeAdded = <Molocclusion />;

    // first time last item ref will be null
    const lastItemHeight = lastItemHeightRef.current.clientHeight
      ? lastItemHeightRef.current.clientHeight + 20
      : 0;
    // const newHeight = currentPageHeight + lastItemHeight;
    const newHeight = 790 + lastItemHeight;

    const fixedItem = getRandomItem();
    // const fixedItem = <TableItems />;

    console.log(lastItemHeight);
    // Check page height
    if (newHeight > defaultPageHeight) {
      const lastItem = pageData[totalPages].pop();

      const lastComponentName = lastItem.type.name;

      console.log(lastComponentName);

      if (lastComponentName === "TableItems") {
        console.log(newHeight);
        setCurrentPageHeight(newHeight);
        TableSplitCheck();
        return;
      }

      // get last component added and add to the next page
      const newPageRange = totalPages + 1;
      setTotalPages(newPageRange);

      newData[newPageRange] = [lastItem];

      setPageData(newData);
      setCurrentPageHeight(60);
      return;
    }

    if (newData[totalPages] === undefined) {
      // newData[totalPages] = [getRandomItem()];
      newData[totalPages] = [fixedItem];
      setPageData(newData);

      setCurrentPageHeight(newHeight);
      return;
    }

    // newData[totalPages].push(getRandomItem());
    newData[totalPages].push(fixedItem);
    setPageData(newData);
    setCurrentPageHeight(newHeight);
  }

  return (
    <div className="App">
      <button className="buttonAddPage" onClick={addItem}>
        Add Item
      </button>
      <div className="document">
        {Array.from(Array(totalPages).keys()).map((page, key) => (
          <div
            className={"page page_number_" + (key + 1)}
            key={page}
            ref={(el) => (pagesRef.current[key + 1] = el)}
          >
            {pageData.length > 0 &&
              pageData[page + 1].map((component, key) => (
                <div key={key} className="component" ref={lastItemHeightRef}>
                  {component}
                </div>
              ))}
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
